package com.davidjonsoftware.main;

import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesModel;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesPresenter;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesView;
import com.davidjonsoftware.main.impl.BuscaminesModelImpl;
import com.davidjonsoftware.main.impl.BuscaminesPresenterImpl;
import com.davidjonsoftware.main.impl.BuscaminesViewImpl;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author David Aleo - Jonathan Alcaide
 */
public class BuscaminesApp extends Application {

    @Override
    public void start(Stage stage) throws IOException {

        BuscaminesView v;
        BuscaminesPresenter p;
        BuscaminesModel m;

        p = new BuscaminesPresenterImpl();
        m = new BuscaminesModelImpl();
        v = new BuscaminesViewImpl(stage);

        p.setView(v);
        p.setModel(m);
        v.setPresenter(p);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

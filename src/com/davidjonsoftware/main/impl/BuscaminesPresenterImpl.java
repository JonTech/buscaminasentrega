package com.davidjonsoftware.main.impl;

import com.davidjonsoftware.main.buscaminas.BuscaminesContract;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesModel;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesModelListener;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesPresenter;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesView;
import com.davidjonsoftware.main.impl.BuscaminesModelImpl.Dificult;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;

public class BuscaminesPresenterImpl implements BuscaminesPresenter, BuscaminesModelListener {

    private BuscaminesModel model;
    private BuscaminesView view;

    @Override
    public void setModel(BuscaminesContract.BuscaminesModel m) {
        this.model = m;
        initModel();
    }

    @Override
    public void setView(BuscaminesContract.BuscaminesView v) {
        this.view = v;
    }

    private void initModel() {
        this.model.addListener(this);
    }

    @Override
    public void toUncover(int pos) {
        model.play(pos);
    }

    @Override
    public Set<Integer> configSizes() {
        return model.getSizes();
    }

    @Override
    public void toRestart(int size, Dificult d) {
        model.start(size, d);

    }

    @Override
    public void overEvent(List<Integer> posMines) {
        // from the model: send it to the view
        if (model.isOver()) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    view.overGame(posMines);
                }

            });
        }
    }

    @Override
    public void winEvent() {
        // from the model: send it to the view
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                view.win();
            }

        });
    }

    @Override
    public void toUncoverEvent() {
        Map<Integer, Integer> boxesDescovered;
        boxesDescovered = model.toUnCovered();

        // from the model: send it to the view
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                view.UnCovered(boxesDescovered);
            }

        });
    }
}

package com.davidjonsoftware.main.impl;

import com.davidjonsoftware.main.buscaminas.Box;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesModelListener;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesPresenter;

import java.util.*;

public class BuscaminesModelImpl implements BuscaminesContract.BuscaminesModel {

    private Set<BuscaminesModelListener> listeners = new LinkedHashSet<BuscaminesModelListener>();
    private Box[][] grid;
    private boolean over = false;

    public enum Dificult {
        EASY, MEDIUM, HARD;
    }
    private List<Integer> posMines = new ArrayList<Integer>();
    private List<Box> boxesDescovered = new ArrayList<Box>();
    private Map<Dificult, Integer> dificultPercents = new HashMap<Dificult, Integer>();
    private int size;
    private Dificult dificult;
    private int pos; //position to discover
    private BuscaminesPresenter presenter;
    private int[] sizes = {10, 12, 14, 16, 18, 20, 22,26};

    @Override
    public void start(int size, Dificult d) {

        //Seteamos el size y la dificultad (metodo?)
        this.size = size;
        this.dificult = d;

        //Limpiamos Map anterior de casillas descubiertas si lo hubiera
        boxesDescovered.clear();

        //Limpiamos posMines
        posMines.clear();

        //Seteamos el porcentage de dificultad (metodo?)
        Integer dificultatInicial = 10; // dificultatInicial final?
        for (Dificult df : Dificult.values()) {
            dificultPercents.put(df, dificultatInicial);
            dificultatInicial += 5;
        }
        //Creamos el tablero
        generatePanelGame(size, getTotalMines());

        //PRUEBA IMPRESION TABLERO
//        for (int i = 0; i < (size + 2); i++) {
//            for (int j = 0; j < (size + 2); j++) {
//                if (grid[j][i] != null) {
//                    if (grid[j][i].getIsMine()) {
//                        System.out.print(" *");
//                    } else {
//                        System.out.print(" " + grid[j][i].getMinesNeighbours());
//                    }
//                } else {
//                    System.out.print(" -");
//                }
//            }
//            System.out.println("");
//        }
        //PRUEBA IMPRESION POSICION MINAS
//        System.out.println(posMines);

    }

    private void generatePanelGame(int size, int mines) {
        Random rnd = new Random();
        int sizeModified = size + 2;
        grid = new Box[sizeModified][sizeModified];
        int boxes = size * size;
        int posX, posY, posBox;

        //Añadimos las casillas normales a la matriz a partir de x: 1, y: 1
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                posY = j + 1;
                posX = i + 1;
                posBox = (i * size) + (j);
                Box box = new Box(posBox);
                grid[posY][posX] = box;
            }
        }

        // Llenamos el tablero de minas
        for (int i = 0; i < mines; i++) {
            int posRandom = rnd.nextInt(boxes);
            posX = (posRandom % size) + 1;
            posY = (posRandom / size) + 1;

            while (posMines.contains(posRandom)) {
                posRandom = rnd.nextInt(boxes);
                posX = (posRandom % size) + 1;
                posY = (posRandom / size) + 1;
            }
            //Seteamos ese box como mina
            Box box = grid[posX][posY];
            posBox = ((posY - 1) * size) + (posX - 1);
            box.setIsMine(true);
            posMines.add(posBox);
        }
        //Hacemos recuento de las minas vecinas
        setMinesNeighbours(posMines);

    }

    private void setMinesNeighbours(List<Integer> posMines) {
        int minesCount = 0;
        int size = grid.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Box box = grid[j][i];
                if (box != null) {
                    for (int k = 0; k < 3; k++) {
                        //Buscamos en la fila anterior empezando desde x-1, y -1 (incrementando x)
                        if (grid[(j - 1)][(i - 1) + k] != null && grid[(j - 1)][(i - 1) + k].getIsMine()) {
                            minesCount++;
                        }
                        //Buscamos en la misma fila empezando desde x-1 (incrementando x)
                        if (grid[j][(i - 1) + k] != null && grid[j][(i - 1) + k].getIsMine()) {
                            minesCount++;
                        }
                        //Buscamos en la fila porterior empezando desde x+1, y+1 (incrementando x)
                        if (grid[(j + 1)][(i - 1) + k] != null && grid[(j + 1)][(i - 1) + k].getIsMine()) {
                            minesCount++;
                        }
                    }
                    box.setMinesNeighbours(minesCount);

                }
                minesCount = 0;
            }
        }
    }

    @Override
    public void play(int pos) {
        this.pos = pos;
        //Limpiamos Map anterior de casillas descubiertas, si lo hubiera.
        boxesDescovered.clear();
        //PRUEBA IMPRESION POSICION
        //System.out.println(pos);

        if (posMines.contains(pos)) {
            sendEvent("overEvent");

        } else if (getRemaining() == 1) {
            sendEvent("winEvent");

        } else {
            int posX = getCol() + 1;
            int posY = getFile() + 1;
            run(posX, posY);
            sendEvent("toUncoverEvent");
        }
    }

    private void run(int col, int row) {
        Box box = grid[col][row];
        if (box != null && !box.getIsMine()) {
            if (!boxesDescovered.contains(box)) {
                boxesDescovered.add(box);
                box.setIsUnCovered(true);

                if (box.getMinesNeighbours() == 0) {
                    //Izquierda x-1
                    run(col - 1, row);
                    //Anterior izq x-1 y-1
                    run(col - 1, row - 1);
                    //Anterior misma col y-1
                    run(col, row - 1);
                    //Anterior der x+1 y-1
                    run(col + 1, row - 1);
                    //Derecha x+1
                    run(col + 1, row);
                    //Posterior izq x-1 y+1
                    run(col - 1, row + 1);
                    //Posterior misma col y+1
                    run(col, row + 1);
                    //Posterior der x+1 y+1
                    run(col + 1, row + 1);
                }
            }
        }
    }

    private void sendEvent(String eventType) {
        Iterator i = listeners.iterator();
        Object o = i.next();
        if (o != null) {
            switch (eventType) {
                case "overEvent":
                    over = true;
                    ((BuscaminesModelListener) o).overEvent(posMines);
                    break;
                case "winEvent":
                    ((BuscaminesModelListener) o).winEvent();
                    break;
                case "toUncoverEvent":
                    ((BuscaminesModelListener) o).toUncoverEvent();
                    break;
            }

        }

    }

    @Override
    public void setPresenter(BuscaminesPresenter p) {
        this.presenter = p;
    }

    @Override
    public boolean addListener(BuscaminesModelListener listener
    ) {
        return listeners.add(listener);
    }

    @Override
    public boolean removeListener(BuscaminesContract.BuscaminesModelListener listener
    ) {
        return listeners.remove(listener);
    }

    @Override
    public Map<Integer, Integer> toUnCovered() {
        Map<Integer, Integer> boxesToUnCover = new HashMap<>();
        for (Box b : boxesDescovered) {
            int position = b.getPos();
            boxesToUnCover.put(b.getPos(), b.getMinesNeighbours());

        }
        return boxesToUnCover;
    }

    @Override
    public int getRemaining() {
        int mines = getTotalMines();
        int boxesUncovered = getBoxesUncovered();
        int totalBoxes = size * size;
        int remainingBoxes = totalBoxes - boxesUncovered - mines;
        return remainingBoxes;
    }

    private int getBoxesUncovered() {
        int count = 0;
        for (int i = 0; i < (size + 2); i++) {
            for (int j = 0; j < (size + 2); j++) {
                if (grid[i][j] != null && grid[i][j].getIsUnCovered()) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public int getTotalMines() {
        int totalBoxes = size * size;
        int minesPercent = dificultPercents.get(dificult);
        return (minesPercent * totalBoxes) / 100;
    }

    @Override
    public boolean isOver() {
        return over;
    }

    @Override
    public Set<Integer> getSizes() {
        TreeSet ts = new TreeSet();
        for (int i = 0; i < sizes.length; i++) {
            ts.add(sizes[i]);
        }
        return ts;
    }

    private int getFile() {
        return pos / size;
    }

    private int getCol() {
        return pos % size;
    }

}

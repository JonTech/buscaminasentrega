package com.davidjonsoftware.main.impl;

import com.davidjonsoftware.main.buscaminas.BuscaminesContract;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesPresenter;
import com.davidjonsoftware.main.buscaminas.BuscaminesContract.BuscaminesView;
import com.davidjonsoftware.main.impl.BuscaminesModelImpl.Dificult;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class BuscaminesViewImpl implements Initializable, BuscaminesView {

    private BuscaminesPresenter presenter;
    private List<MyButton> boxesList = new ArrayList();
    private List<Text> txtList = new ArrayList();
    private EventHandler<MouseEvent> mouseHandler;
    private EventHandler<ActionEvent> itemHandler;

    private boolean start = true;
    private int size = 10;
    private Dificult dificult = Dificult.EASY;
    private final double PREF_HEIGHT = 78.0;
    private final double PREF_WIDTH = 18.0;

    private final Image FLAG = new Image((getClass().getResourceAsStream("/com/davidjonsoftware/main/resources/images/flag.png")), 14.0, 20.0, false, false);
    private final Image MINE = new Image((getClass().getResourceAsStream("/com/davidjonsoftware/main/resources/images/mine.png")), 14.0, 20.0, false, false);
    private final Image APP_ICON = new Image((getClass().getResourceAsStream("/com/davidjonsoftware/main/resources/images/appIcon.png")), 22.0, 22.0, false, false);
    private final AudioClip WIN = new AudioClip(getClass().getResource("/com/davidjonsoftware/main/resources/audio/win.wav").toString());
    private final AudioClip EXPLOSION = new AudioClip(getClass().getResource("/com/davidjonsoftware/main/resources/audio/explosionMine.wav").toString());
    private final AudioClip CLICK = new AudioClip(getClass().getResource("/com/davidjonsoftware/main/resources/audio/click.wav").toString());
    private List<String> sizes;
    private Scene scene;
    private Stage stage;
    private boolean musicOn = false;
    private boolean menuEdited = false;
    private String difficulty[] = {"Easy", "Medium", "Hard"};
    private String sound[] = {"On", "Off"};
    private Color[] colors = {Color.BLUE, Color.GREEN, Color.RED, Color.PURPLE, Color.BROWN, Color.CHARTREUSE, Color.DEEPPINK, Color.CRIMSON};

    @FXML
    private GridPane gridPane;
    @FXML
    private MenuBar menuBar;

    public BuscaminesViewImpl(Stage stage) {
        initUI(stage);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void setPresenter(BuscaminesContract.BuscaminesPresenter p) {
        this.presenter = p;
    }

    // UI
    private void initUI(Stage stage) {
        this.stage = stage;
        FXMLLoader loader = new FXMLLoader();
        loader.setController(this);
        loader.setLocation(getClass().getResource("/com/davidjonsoftware/main/resources/layout/buscamines.fxml"));
        stage.getIcons().add(APP_ICON);
        Parent root;

        try {
            root = loader.load();
            scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/com/davidjonsoftware/main/resources/styles/buscamines.css").toExternalForm());

            //Handlers
            itemHandler = new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    MenuItem item = (MenuItem) event.getSource();
                    String txt = item.getText();

                    switch (txt) {
                        case "Exit":
                            Platform.exit();
                            break;
                        case "Easy":
                            dificult = Dificult.EASY;
                            presenter.toRestart(size, dificult);
                            setNewStage(stage, scene, root);
                            start = false;
                            break;
                        case "Medium":
                            dificult = Dificult.MEDIUM;
                            presenter.toRestart(size, dificult);
                            setNewStage(stage, scene, root);
                            start = false;
                            break;
                        case "Hard":
                            dificult = Dificult.HARD;
                            presenter.toRestart(size, dificult);
                            setNewStage(stage, scene, root);
                            start = false;
                            break;
                        case "On":
                            musicOn = true;
                            break;
                        case "Off":
                            musicOn = false;
                            break;
                        case "About":
                            Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("About");
                            alert.setHeaderText("Version & Authors");
                            alert.setContentText("Version:"
                                    + "\n1.1.6\n"
                                    + "\nDevelopers:"
                                    + "\n-David Aleo"
                                    + "\n-Jonathan Alcaide");
                            alert.showAndWait();
                            break;
                        case "Help Contents":
                            alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Help contents");
                            alert.setHeaderText("¿Como se juega?");
                            alert.setContentText("El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.\n" +
                                    "\n" +
                                    "Algunas casillas tienen un número, el cual indica la cantidad de minas que hay en las casillas circundantes. Así, si una casilla tiene el número 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o borde) hay 3 con minas y 5 sin minas. Si se descubre una casilla sin número indica que ninguna de las casillas vecinas tiene mina y éstas se descubren automáticamente.\n" +
                                    "\n" +
                                    "Si se descubre una casilla con una mina se pierde la partida.\n" +
                                    "\n" +
                                    "Se puede poner una marca en las casillas que el jugador piensa que hay minas para ayudar a descubrir las que están cerca.");
                            alert.showAndWait();
                            break;
                        default:
                            //Default es el tamaño del panel de juego (10x10, 12x12, etc)
                            String sizeText = txt.substring(0, 5);
                            if (sizes.contains(sizeText)) {
                                String sizeItem = txt.substring(0, 2);
                                int sizeNum = Integer.parseInt(sizeItem);
                                //Saca el size del evento y resetea tablero
                                size = sizeNum;
                                presenter.toRestart(size, dificult);
                                setNewStage(stage, scene, root);
                                start = false;
                            }
                            break;
                    }

                }
            };
            mouseHandler = new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (musicOn) {
                        CLICK.play();
                    }
                    //Le pasamos al presenter para que pase al modelo el size y la dificultad
                    if (start) {
                        presenter.toRestart(size, dificult);
                        start = false;
                    }
                    MyButton myButton = (MyButton) event.getSource();
                    MouseButton mouseButton = event.getButton();
                    int pos = myButton.getPos();

                    if (mouseButton == MouseButton.PRIMARY) {
                        if (!myButton.isFlag()) {
                            presenter.toUncover(pos);

                        }

                    } else if (!myButton.isFlag()) {
                        myButton.setFlag(true);
                        myButton.setGraphic(new ImageView(FLAG));
                        showStage(stage, scene);

                    } else {
                        myButton.setFlag(false);
                        myButton.setGraphic(null);
                        showStage(stage, scene);
                    }
                }
            };

            //Añadimos eventos a los Menu items y los asociamos a su handler (itemHandler)
            setHandlerToEditMenu();
            setHandlersToMenuItems();
            setRadioMenuItems("Sound");
            setRadioMenuItems("Difficulty");

            //Seleccionamos los items de los menuItems Difficulty y Sound
            //Creamos la pantalla
            setNewStage(stage, scene, root);

        } catch (IOException e) {
            throw new RuntimeException("loading buscamines.xml", e);
        }

    }

    private void setNewStage(Stage stage, Scene scene, Parent root) {
        //Creamos una lista de botones segun el size
        fillListOfBoxes();

        //Llenamos el GridPane con los botones del boxesList
        fillGridPanel();

        //Adaptamos el gridPane (columConstraint, rowConstraints) al size nuevo
        setColumnsAndRows();

        //Añadimos eventos a los botones asociandolo al mouseHandler
        setHandlersToButtons();

        //Cambiamos el tamaño del panel y lo adaptamos al nuevo size
        setStageSizes(stage);

        //Mostramos pantalla
        showStage(stage, scene);
    }

    public void showStage(Stage stage, Scene scene) {
        stage.setScene(scene);
        stage.setTitle("Buscamines");
        stage.show();
    }

    public void setColumnsAndRows() {

        int numColsInPane = gridPane.getColumnConstraints().size();
        int dif;
        if (numColsInPane < size) {
            dif = size - numColsInPane;
            for (int i = 0; i < dif; i++) {
                gridPane.getColumnConstraints().add(new ColumnConstraints(35));
                gridPane.getRowConstraints().add(new RowConstraints(35));
            }
        }
    }

    public void setStageSizes(Stage stage) {
        //Cambiamos el tamaño de la ventana segun size también el menu bar
        double total = 0.0;
        for (int i = 0; i < size; i++) {
            total += 35.0;
        }
        stage.setMinHeight(PREF_HEIGHT + total);
        stage.setMaxHeight(PREF_HEIGHT + total);
        stage.setMinWidth(PREF_WIDTH + total);
        stage.setMaxWidth(PREF_WIDTH + total);
        menuBar.setMinWidth(PREF_WIDTH + total);
        menuBar.setMaxWidth(PREF_WIDTH + total);
    }

    @Override
    public void UnCovered(Map<Integer, Integer> boxesDescovered) {
        int size = boxesDescovered.size();
        HashMap<Integer, Integer> hm = (HashMap) boxesDescovered;
        for (MyButton mb : boxesList) {
            if (hm.containsKey(mb.getPos())) {
                if (mb.isFlag()) {
                    mb.setGraphic(null);
                }
                //Si la casilla contiene 0 minas alrededor
                if (hm.get(mb.getPos()) == 0) {
                    mb.setDisable(true);

                } else {
                    mb.setDisable(true);
                    mb.getStyleClass().add("buttonNumber");
                    setTextAndColor(mb, hm.get(mb.getPos()));
                }
            }
        }
        showStage(stage, scene);
    }

    private void setTextAndColor(MyButton button, int num) {
        String text = "" + num;
        button.setText(text);
        button.setTextFill(colors[num - 1]);
    }

    @Override
    public void overGame(List<Integer> posMines) {
        for (Integer mine : posMines) {
            for (MyButton mb : boxesList) {
                if (mb.getPos() == mine) {
                    mb.setGraphic(new ImageView(MINE));
                }
            }
        }
        showStage(stage, scene);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Game Over");
        alert.setHeaderText("Mine exploded");
        alert.setContentText("You clicked on a mine!");
        if (musicOn) {
            EXPLOSION.play();
        }
        alert.showAndWait();
        start = true;
        setNewStage(stage, scene, gridPane);
    }

    @Override
    public void win() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Win");
        alert.setHeaderText("Congratulations");
        alert.setContentText("You found all the mines!");
        if (musicOn) {
            WIN.play();
        }
        alert.showAndWait();
        start = true;
        setNewStage(stage, scene, gridPane);

    }

    class MyButton extends Button {

        private boolean flag;
        private int pos;

        public int getPos() {
            return pos;
        }

        public MyButton(int pos) {
            flag = false;
            this.pos = pos;
        }

        public boolean isFlag() {
            return flag;
        }

        public void setFlag(boolean flag) {
            this.flag = flag;
        }

    }

    private void fillListOfBoxes() {
        //Limpiamos la lista
        boxesList.clear();

        int totalBoxes = size * size;
        for (int i = 0; i < totalBoxes; i++) {
            MyButton button = new MyButton(i);
            button.getStyleClass().add("button");
            boxesList.add(button);
        }
    }

    private void fillGridPanel() {
        //Limpiamos el gridPane
        cleanGridPane();
        int index = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                MyButton button = boxesList.get(index++);
                gridPane.add(button, j, i);
            }
        }
    }

    private void cleanGridPane() {
        int childrens = gridPane.getChildren().size();
        for (int i = 0; i < childrens; i++) {
            gridPane.getChildren().remove(0);
        }
    }

    private void setRadioMenuItems(String nameMenu) {
        String[] menuNames;
        switch (nameMenu) {
            case "Edit":
                menuNames = new String[sizes.size()];
                int count = 0;
                for (String s : sizes) {
                    menuNames[count++] = s;
                }
                break;
            case "Difficulty":
                menuNames = difficulty;
                break;
            case "Sound":
                menuNames = sound;
                break;
            default:
                throw new AssertionError();
        }
        for (Menu menu : menuBar.getMenus()) {
            String menuText = menu.getText();
            if (menuText.equals(nameMenu)) {
                ToggleGroup group = new ToggleGroup();

                for (int i = 0; i < menuNames.length; i++) {
                    RadioMenuItem rmi = new RadioMenuItem(menuNames[i]);
                    rmi.setUserData(menuNames[i]);
                    //Lo metemos en el toggleGroup para que solo se seleccione una opción
                    rmi.setToggleGroup(group);
                    //Lo metemos en el menu
                    menu.getItems().add(rmi);
                    //Le metemos el handler
                    rmi.addEventHandler(ActionEvent.ACTION, itemHandler);
                    //Dejamos seleccionada la opción el RadioItemButton deseada
                    if ((i == 0 && !"On".equals(menuNames[i])) || "Off".equals(menuNames[i])) {
                        rmi.setSelected(true);
                    }
                }
            }
        }
    }

    private void setHandlersToMenuItems() {
        for (Menu menu : menuBar.getMenus()) {
            String menuText = menu.getText();
            if (menuText.equals("File") || menuText.equals("Help")) {
                for (MenuItem i : menu.getItems()) {
                    i.addEventHandler(ActionEvent.ACTION, itemHandler);
                }
            }
        }
    }

    private void setHandlerToEditMenu() {
        for (Menu menu : menuBar.getMenus()) {
            String menuName = menu.getText();
            if (menuName.equals("Edit")) {
                menu.setText(menuName);
                menu.addEventHandler(Menu.ON_SHOWING, event -> setEditMenu());
            }
        }
    }

    private void setEditMenu() {

        if (!menuEdited) {
            /*Limpiamos antes el menu Edit ja que por culpa del ON_Showing necesita
            al menos un elemento para poder ejecutar el evento.*/
            cleanEditMenu();
            //Cogemos los tamaños del modelo y los metemos en un arraylist
            fillSizesFromModel();
            //Creamos y metemos los RadioMenuItems en su menu
            setRadioMenuItems("Edit");
            //Lo ponemos a true asi solo entra la primera vez
            menuEdited = true;
        }
    }

    private void cleanEditMenu() {
        for (Menu m : menuBar.getMenus()) {
            String name = m.getText();
            if ("Edit".equals(name)) {
                int childrens = m.getItems().size();
                for (int i = 0; i < childrens; i++) {
                    m.getItems().remove(0);
                }
            }
        }
    }

    private void fillSizesFromModel() {
        TreeSet ts = (TreeSet) presenter.configSizes();
        sizes = new ArrayList();
        Iterator i = ts.iterator();
        while (i.hasNext()) {
            Integer num = (Integer) i.next();
            String name = num + "x" + num;
            sizes.add(name);
        }

    }

    private void setHandlersToButtons() {
        for (Node node : gridPane.getChildren()) {
            if (node instanceof MyButton) {
                //((Button) node).addEventHandler(ActionEvent.ACTION, buttonHandler);
                ((MyButton) node).setOnMouseClicked(mouseHandler);
            }
        }
    }
}
